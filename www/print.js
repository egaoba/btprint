var BTPrintLoader = function(require, exports, module) {

    var exec = require("cordova/exec");

    function PrintPlugin(require, exports, module) {}

    PrintPlugin.prototype.conn = function(macAddress,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }
        exec(successCallback, errorCallback, 'PrintPlugin', 'conn', [macAddress]);
    };

    PrintPlugin.prototype.printText = function(contStr,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }
        exec(successCallback, errorCallback, 'PrintPlugin', 'printText', [contStr]);
    };
    
    // 打印文字，第2、3参数类型为整数
    PrintPlugin.prototype.printTextV2 = function(contStr,fontSize,isBold,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }
        exec(successCallback, errorCallback, 'PrintPlugin', 'printTextV2', [contStr,fontSize,isBold]);
    };
    
    // 打印图片
    PrintPlugin.prototype.printPic = function(picUrl,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }
        exec(successCallback, errorCallback, 'PrintPlugin', 'printPic', [picUrl]);
    };

    PrintPlugin.prototype.printBarcode = function(codeStr,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        exec(successCallback, errorCallback, 'PrintPlugin', 'printBarcode', [codeStr]);
    };
    
    PrintPlugin.prototype.printQRCode = function(codeStr,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        exec(successCallback, errorCallback, 'PrintPlugin', 'printQRCode', [codeStr]);
    };
    
    PrintPlugin.prototype.getDevices = function(successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        exec(successCallback, errorCallback, 'PrintPlugin', 'getBoundDevices', []);
    };

    var printPlugin = new PrintPlugin();
    module.exports = printPlugin;

};

BTPrintLoader(require, exports, module);
cordova.define("cordova/plugin/PrintPlugin", BTPrintLoader);